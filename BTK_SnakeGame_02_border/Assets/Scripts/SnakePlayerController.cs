﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

/// <summary>
/// Snake player controller. 
/// Handles Input and Player Specific Gameplay
/// </summary>
public class SnakePlayerController : MonoBehaviour {

	/// <summary>
	/// The current game level.
	/// </summary>
	[SerializeField] private GameObject currentGameLevel;

	/// <summary>
	/// The size of the grid. This is a static varibale because it is fixed and should be accesible from a lot of classes.
	/// </summary>
	static float m_gridSize = 0.5f;

	/// <summary>
	/// flag to disable input
	/// </summary>
	private bool m_stopInput = false;

	/// <summary>
	/// flag to check if the snake only is a head segment or has already body segments
	/// </summary>
	private bool m_hasTail = false;

	/// <summary>
	/// the last direction / input was goven
	/// </summary>
	private Vector3 lastInputDirection;

	/// <summary>
	/// Getet Function for the grid size
	/// </summary>
	/// <returns>The grid size.</returns>
	static public float GetGridSize()
	{
		return m_gridSize;
	}

	/// <summary>
	/// Update is called once per frame
	/// </summary>
	void Update () 
	{
		//Check for key input and assign a direction to move to
		if (m_stopInput)
		{
			return;
		}
		else if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
		{
			AssignMovementDirection(Vector3.up);
		}
		else if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
		{
			AssignMovementDirection(Vector3.down);
		}
		else if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
		{
			AssignMovementDirection(Vector3.left);
		}
		else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
		{
			AssignMovementDirection(Vector3.right);
		}
	}

	/// <summary>
	/// Assigns the direction to the movement controller
	/// </summary>
	/// <param name="direction">Direction.</param>
	void AssignMovementDirection(Vector3 direction)
	{
		//send the direction to the movement controller
		SendMessage("MoveIntoDirection",direction);

		///rembember the last valid direction input
		lastInputDirection = direction;
	}
}
