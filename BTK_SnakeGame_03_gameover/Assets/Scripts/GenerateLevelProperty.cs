﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Generate level property - used to spawn border and pickup objects
/// </summary>
public class GenerateLevelProperty : MonoBehaviour {

	/// <summary>
	/// Template to generate border objects
	/// </summary>
	[SerializeField] private GameObject m_borderObjectTemplate;

	/// <summary>
	/// Template to generate pickup objects
	/// </summary>
	[SerializeField] private GameObject m_pickupObjectTemplate;

	/// <summary>
	/// The width of the border rectangle ( In border objects (
	/// </summary>
	[SerializeField] private int m_widthOfBorder;

	/// <summary>
	/// The length of the border rectangle ( In border objects (
	/// </summary>
	[SerializeField] private int m_lengthOfBorder;

	/// <summary>
	/// initialization of the border rectangle
	/// </summary>
	void Start () 
	{
		//calculate Y position for the top row and X position for right colllum
		//only works with uneven length / width
		float topRowPositionY = ((float)m_lengthOfBorder-1) / 2f * SnakePlayerController.GetGridSize();
		float rightCollumPositionX = ((float)m_widthOfBorder-1) / 2f * SnakePlayerController.GetGridSize();

		int index = 0;

		//iterate through width and create 2 border objects for each step
		while (index < m_widthOfBorder) 
		{
			float rowPositionX = (index * SnakePlayerController.GetGridSize()) - rightCollumPositionX;

			CreateNewBorderObject(new Vector3(rowPositionX,topRowPositionY,0));
			CreateNewBorderObject(new Vector3(rowPositionX,-topRowPositionY,0));

			index++;
		}

		index = 1;

		//iterate through length and create 2 border objects for each step
		while (index < m_lengthOfBorder-1) 
		{
			float collumPositionY = (index * SnakePlayerController.GetGridSize()) - topRowPositionY;

			CreateNewBorderObject(new Vector3(rightCollumPositionX,collumPositionY,0));
			CreateNewBorderObject(new Vector3(-rightCollumPositionX,collumPositionY,0));

			index++;
		}
	}

	/// <summary>
	/// Creates a new border object on a certain position
	/// </summary>
	/// <param name="objectPosition">Object position.</param>
	void CreateNewBorderObject(Vector3 objectPosition)
	{
		GameObject borderObject;

		borderObject = GameObject.Instantiate (m_borderObjectTemplate);
		borderObject.transform.position = objectPosition;

		borderObject.name = "BorderObject_" + objectPosition.x + "_" + objectPosition.y;
	}
}
