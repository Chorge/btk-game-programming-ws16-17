﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

/// <summary>
/// Snake player controller. 
/// Handles Input and Player Specific Gameplay
/// </summary>
public class SnakePlayerController : MonoBehaviour {

	/// <summary>
	/// The current game level.
	/// </summary>
	[SerializeField] private GameObject currentGameLevel;

	/// <summary>
	/// The size of the grid. This is a static varibale because it is fixed and should be accesible from a lot of classes.
	/// </summary>
	static float m_gridSize = 0.5f;

	/// <summary>
	/// flag to disable input
	/// </summary>
	private bool m_stopInput = false;

	/// <summary>
	/// flag to check if the snake only is a head segment or has already body segments
	/// </summary>
	private bool m_hasTail = false;

	/// <summary>
	/// the last direction / input was goven
	/// </summary>
	private Vector3 lastInputDirection;

	/// <summary>
	/// Getet Function for the grid size
	/// </summary>
	/// <returns>The grid size.</returns>
	static public float GetGridSize()
	{
		return m_gridSize;
	}

	/// <summary>
	/// Update is called once per frame
	/// </summary>
	void Update () 
	{
		//Check for key input and assign a direction to move to
		if (m_stopInput)
		{
			return;
		}
		else if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
		{
			AssignMovementDirection(Vector3.up);
		}
		else if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
		{
			AssignMovementDirection(Vector3.down);
		}
		else if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
		{
			AssignMovementDirection(Vector3.left);
		}
		else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
		{
			AssignMovementDirection(Vector3.right);
		}
	}

	/// <summary>
	/// Assigns the direction to the movement controller
	/// </summary>
	/// <param name="direction">Direction.</param>
	void AssignMovementDirection(Vector3 direction)
	{
		//do not move in the opposite direction of current movement if already has a snake body
		if (m_hasTail && direction == -lastInputDirection) 
		{
			return;
		}

		//send the direction to the movement controller
		SendMessage("MoveIntoDirection",direction);

		///rembember the last valid direction input
		lastInputDirection = direction;
	}

	/// <summary>
	/// Called by the the collision system, overwritten to trigger pickups and gameover
	/// </summary>
	/// <param name="coll">Coll.</param>
	void OnCollisionEnter2D(Collision2D coll) 
	{
		///trigger gameover when colliding with wall or the snake body
		if (coll.gameObject.tag == "Danger") 
		{
			GameOver();
		}
	}

	/// <summary>
	/// Pauses the game and restarts it in 5 seconds
	/// </summary>
	void GameOver()
	{
		Camera.main.backgroundColor = Color.black;
		m_stopInput = true;
		SendMessage("StopMovement");

		///Delays the RestartGame funktion with 5 seconds 
		Invoke ("RestartGame", 5f);
	}

	/// <summary>
	/// Reloads the current gamelevel
	/// </summary>
	public void RestartGame()
	{
		int scene = SceneManager.GetActiveScene().buildIndex;
		SceneManager.LoadScene(scene, LoadSceneMode.Single);
	}

}
