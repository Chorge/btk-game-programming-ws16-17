﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Snake body controller - controlls the snake body parts
/// </summary>
public class SnakeBodyController : MonoBehaviour {

	/// <summary>
	/// template to generate new body part objects
	/// </summary>
	[SerializeField] private GameObject m_bodySegmentTemplate;

	/// <summary>
	/// The next body segment following after this segement
	/// </summary>
	GameObject m_nextBodySegment;

	/// <summary>
	/// The number of the segment.
	/// </summary>
	int m_indexOfSegment;

	/// <summary>
	/// Grows the snake, creates a new body part or tell the next body part to grow a snake
	/// </summary>
	/// <param name="direction">Direction.</param>
	public void GrowSnake(Vector3 direction)
	{
		if (m_nextBodySegment == null) 
		{
			///if body part dos not have a tail, grow a new one
			GrowTailSegement(direction);
		} 
		else 
		{
			///tell the next segment to grow a body part in a certain direction
			m_nextBodySegment.SendMessage("GrowSnake",direction);
		}
	}

	/// <summary>
	/// Creates a tail segement in a certain direction
	/// </summary>
	/// <param name="direction">Direction.</param>
	void GrowTailSegement(Vector3 direction)
	{  
		///position to spawn the next segment
		Vector3 tailPosition = transform.position - direction;
		///Create a new instance for the next body segment
		m_nextBodySegment = GameObject.Instantiate (m_bodySegmentTemplate);
		m_nextBodySegment.transform.position = tailPosition;
		m_nextBodySegment.name = "BodySegment_" + m_indexOfSegment+1;

		m_nextBodySegment.SendMessage("SetIndexOfSegement", m_indexOfSegment+1);
		m_nextBodySegment.SendMessage("MoveToPosition", transform.position);
	}

	/// <summary>
	/// Moves the next body segement to a position
	/// </summary>
	/// <param name="desiredPosition">Desired position.</param>
	public void MoveNextBodySegement(Vector3 desiredPosition)
	{
		if (m_nextBodySegment != null) 
		{
			m_nextBodySegment.SendMessage("MoveToPosition", desiredPosition);
		} 
	}

	/// <summary>
	/// Stops the next body segement.
	/// </summary>
	public void StopNextBodySegement()
	{
		if (m_nextBodySegment != null) 
		{
			m_nextBodySegment.SendMessage("StopMovement");
		} 
	}

	/// <summary>
	/// Sets the index of segement (for unqiue naming)
	/// </summary>
	/// <param name="index">Index.</param>
	public void SetIndexOfSegement(int index)
	{
		m_indexOfSegment = index;
	}
}
