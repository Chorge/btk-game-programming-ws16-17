﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Snake movement controller. Handle movement of snake head and body parts
/// </summary>
public class SnakeMovementController : MonoBehaviour {

	/// <summary>
	/// The movement speed.
	/// </summary>
	[SerializeField] private float m_movementSpeed;

	/// <summary>
	/// The current direction the snake head is moving
	/// </summary>
	private Vector3 m_currentDirection;

	/// <summary>
	/// The desired direction the snake head wants to move
	/// </summary>
	private Vector3 m_desiredDirection;

	/// <summary>
	/// The next position on the grid
	/// </summary>
	private Vector3 m_nextGridPosition;


	/// <summary>
	/// The last position on the grid
	/// </summary>
	private Vector3 m_lastGridPosition;

	/// <summary>
	/// The target position the snake body parts wants to move
	/// </summary>
	private Vector3 m_targetPosition;


	/// <summary>
	/// Flag to diable moving.
	/// </summary>
	private bool m_stopMoving;

	/// <summary>
	/// Update is called every  fixed framerate frame
	/// </summary>
	void FixedUpdate () 
	{
		if (m_stopMoving) 
		{
			return;
		}

		Vector3 desiredPosition;

		///if we do not have a target position move towards the desired direction ( used for the snake head )
		if (m_targetPosition == Vector3.zero) 
		{	

			///move to next position on the grid
			desiredPosition = Vector3.MoveTowards (transform.position, m_nextGridPosition, m_movementSpeed * Time.deltaTime);

			if (desiredPosition == transform.position)
			{
				//if we reach the position change the direction if needed
				m_currentDirection = m_desiredDirection;
				///get the next position on the grid
				CalculateNextGridPosition ();
				///tell the snake body controller to move the next segment to the last grid position
				SendMessage ("MoveNextBodySegement", m_lastGridPosition);
			}
		} 
		else 
		{
			///move to the desired position
			desiredPosition = Vector3.MoveTowards (transform.position, m_targetPosition, m_movementSpeed * Time.deltaTime);

			if (desiredPosition == transform.position)
			{
				///tell the snake body controller to move the next segment to this position
				SendMessage ("MoveNextBodySegement", m_targetPosition);
			}
		}

		///set the position directly to the transfrom
		transform.position = desiredPosition;
	}

	/// <summary>
	/// Moves the into a direction, this function is called from the player controller
	/// </summary>
	/// <param name="direction">Direction.</param>
	public void MoveIntoDirection(Vector3 direction)
	{
		///in the beginnging move in any direction
		if (m_currentDirection == Vector3.zero) 
		{
			m_currentDirection = direction;
			m_desiredDirection = direction;
		}
		else if (m_currentDirection != direction) 
		{
			///only set the desired direction
			m_desiredDirection = direction;
		}
	}

	/// <summary>
	/// Moves to a position. This function is called from the snake body controller
	/// </summary>
	/// <param name="position">Position.</param>
	public void MoveToPosition(Vector3 position)
	{
		m_targetPosition = position;
	}

	/// <summary>
	/// Stops the movement and tells the next body segments to stop movement too
	/// </summary>
	public void StopMovement()
	{
		m_stopMoving = true;

		SendMessage ("StopNextBodySegement");

	}

	/// <summary>
	/// Calculates the next grid position in a certain direction
	/// </summary>
	void CalculateNextGridPosition()
	{
		///remember the current grid position
		m_lastGridPosition = m_nextGridPosition;

		///get a position in the current movement direction 
		Vector3 gridPostion = transform.position + (m_currentDirection * SnakePlayerController.GetGridSize());

		///reduce x and y coordinate by exact the fraction of the gridsize
		gridPostion.x = gridPostion.x - gridPostion.x % SnakePlayerController.GetGridSize();
		gridPostion.y = gridPostion.y - gridPostion.y % SnakePlayerController.GetGridSize();
		m_nextGridPosition = gridPostion;
	}
}
